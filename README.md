- **Untitled.ipynb**
メイン
- **new_drug_data.tsv.gz**
生成データ
- MEDIS20201130.TXT
MEDIS HOTコードマスタ
- tokuyaku_20201124.xls
特定薬剤管理指導加算等の算定対象となる薬剤一覧
- tp20201211-01_05.xlsx
各先発医薬品の後発医薬品の有無に関する情報
- pmda_drug_data_md_20201009.tsv.gz
Datack drug data
- y.zip
診療報酬情報提供サービス 医薬品マスタ
- check.tsv
整合性チェック用データ（既存の薬歴システム由来）