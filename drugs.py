#%%
import pandas as pd
import csv

# %%
Y_PATH = './y.zip'
MEDIS_PATH = './MEDIS20201130.TXT'
PMDA_PATH = './pmda_drug_data_md_20201009.tsv.gz'
# %%
d_raw = pd.read_csv(PMDA_PATH, sep='\t', dtype='string')
# %%
y_raw = pd.read_csv(
        Y_PATH, encoding="cp932", header=None, dtype='string',
        names=[
            '変更区分',
            'マスター種別',
            '個別レセ電コード',
            '医薬品名・規格名_漢字有効桁数',
            '医薬品名・規格名_漢字名称',
            '医薬品名・規格名_カナ有効桁数',
            '医薬品名・規格名_カナ名称',
            '単位_コード',
            '単位_漢字有効桁数',
            '単位_漢字名称',
            '新又は現金額_金額種別',
            '新又は現金額',
            '予備1',
            '麻薬・毒薬・覚せい剤原料・向精神薬',
            '神経破壊剤',
            '生物学的製剤',
            '後発品',
            '予備2',
            '歯科特定薬剤',
            '造影（補助）剤',
            '注射容量',
            '収載方式等識別',
            '商品名等関連',
            '旧金額_旧金額種別',
            '旧金額_旧金額',
            '漢字名称変更区分',
            'カナ名称変更区分',
            '剤形',
            '予備3',
            '変更年月日',
            '廃止年月日',
            '薬価基準収載医薬品コード',
            '公表順序番号',
            '経過措置年月日又は商品名医薬品コード使用期限',
            '基本漢字名称']
    )
y = (y_raw
    ).drop_duplicates(
        ['薬価基準収載医薬品コード', '後発品']
    )
y.columns
# %%
h_cols = {
    '個別医薬品コード':'YJコード',
    '薬価基準収載医薬品コード':'薬価基準収載医薬品コード',
    'レセプト電算処理システムコード（１）':'個別レセ電コード',
    'レセプト電算処理システムコード（２）':'統一レセ電コード',
    '販売名':'HOT販売名',
}
h_raw = pd.read_csv(MEDIS_PATH, encoding="cp932", dtype='string')
h = (h_raw
    ).rename(
        h_cols, axis=1
    ).drop_duplicates(
        h_cols.values()
    )
h.columns
#%%
yjs = d_raw['YJコード'].append(h['YJコード']).drop_duplicates().dropna()
print(len(yjs))
# %%
res = (pd.DataFrame(yjs,columns=['YJコード'])
    ).merge(
        d_raw,
        on='YJコード',
        how='left'
    ).merge(
        h,
        on='YJコード',
        how='left'
    ).merge(
        y,
        on=['個別レセ電コード','薬価基準収載医薬品コード'],
        how='left'
    ).rename(
        {
            'YJコード':'yj_code',
            '一般名':'general_name',
        }, axis=1)
# %%
res['yj_code_8'] = res['yj_code'].str[0:8]
res['product_name'] = res['基本漢字名称'].fillna(res['HOT販売名']).fillna(res['販売名'])
res['drug_type'] = res['後発品'].apply(
    lambda x: {'1': 'generic', '0': 'reference'}.get(x, 'reference')
)
res['general_name'] = res['general_name'].fillna('NA')
# %%
res.columns
# %%
res[['yj_code', 'yj_code_8','drug_type','general_name','product_name']].to_csv('./drugs.csv', index=False)
# %%
