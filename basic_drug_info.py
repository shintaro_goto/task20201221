#%%
import pandas as pd

TOKUYAKU_PATH = './tokuyaku_20201210.xls'
PMDA_PATH = './pmda_drug_data_md_20201009.tsv.gz'
UMU_PATH = './tp20201211-01_05.xlsx'
Y_PATH = './y.zip'
MEDIS_PATH = './MEDIS20201130.TXT'
DRUG100_PATH = './drug100.csv'

#%%
d_raw = pd.read_csv(PMDA_PATH, sep='\t', dtype='string')
d100 = pd.read_csv(DRUG100_PATH, dtype='string')

#%%
u_raw = pd.read_excel(UMU_PATH)
u = (u_raw
    ).drop_duplicates(
        ['薬価基準収載医薬品コード', '各先発医薬品の後発医薬品の有無に関する情報']
    )

u['算定対象となる後発品'] = u['各先発医薬品の後発医薬品の有無に関する情報'].apply(
        lambda x:{ '3':'TRUE' }.get(str(x), '')
    )
u['先発品'] = u['各先発医薬品の後発医薬品の有無に関する情報'].apply(
        lambda x:{ '1':'先発品', '2':'先発品', '☆':'先発品' }.get(str(x), '')
    )
u['後発品がある先発品'] = u['各先発医薬品の後発医薬品の有無に関する情報'].apply(
        lambda x:{ '2':'TRUE' }.get(str(x), '')
    )

u.columns

#%%
t_raw = pd.read_excel(TOKUYAKU_PATH, header=1, dtype='string')
t = (t_raw
    ).rename(
        { 
            '薬価基準コード':'薬価基準収載医薬品コード',
            '医薬品コード':'個別レセ電コード',
        },
        axis=1
    ).drop_duplicates(
        ['個別レセ電コード', '算定対象となる薬剤']
    )
t['ハイリスク算定対象'] = t['算定対象となる薬剤'].apply(
    lambda x: {'○':True}.get(x, False)
)
t.columns

#%%
y_raw = pd.read_csv(
        Y_PATH, encoding="cp932", header=None, dtype='string',
        names=[
            '変更区分',
            'マスター種別',
            '個別レセ電コード',
            '医薬品名・規格名_漢字有効桁数',
            '医薬品名・規格名_漢字名称',
            '医薬品名・規格名_カナ有効桁数',
            '医薬品名・規格名_カナ名称',
            '単位_コード',
            '単位_漢字有効桁数',
            '単位_漢字名称',
            '新又は現金額_金額種別',
            '新又は現金額',
            '予備1',
            '麻薬・毒薬・覚せい剤原料・向精神薬',
            '神経破壊剤',
            '生物学的製剤',
            '後発品',
            '予備2',
            '歯科特定薬剤',
            '造影（補助）剤',
            '注射容量',
            '収載方式等識別',
            '商品名等関連',
            '旧金額_旧金額種別',
            '旧金額_旧金額',
            '漢字名称変更区分',
            'カナ名称変更区分',
            '剤形',
            '予備3',
            '変更年月日',
            '廃止年月日',
            '薬価基準収載医薬品コード',
            '公表順序番号',
            '経過措置年月日又は商品名医薬品コード使用期限',
            '基本漢字名称']
    )
y = (y_raw
    ).drop_duplicates(
        ['薬価基準収載医薬品コード', '後発品']
    )
y['後発品'] = y['後発品'].apply(
    lambda x: {'1': '後発品'}.get(x, '')
)
y.columns
#%%
h_cols = {
    '個別医薬品コード':'YJコード',
    '薬価基準収載医薬品コード':'薬価基準収載医薬品コード',
    'レセプト電算処理システムコード（１）':'個別レセ電コード',
    'レセプト電算処理システムコード（２）':'統一レセ電コード',
}
h_raw = pd.read_csv(MEDIS_PATH, encoding="cp932", dtype='string')
h = (h_raw
    ).rename(
        h_cols, axis=1
    ).drop_duplicates(
        h_cols.values()
    ).drop(
        '販売名',
        axis=1
    )
h.columns

#%%
res = (d100
    ).merge(
        d_raw,
        on='YJコード',
        how='left'
    ).merge(
        h,
        on='YJコード',
        how='left'
    ).merge(
        y,
        on=['個別レセ電コード','薬価基準収載医薬品コード'],
        how='left'
    ).merge(
        u[['薬価基準収載医薬品コード', '算定対象となる後発品', '先発品', '後発品がある先発品']], 
        on='薬価基準収載医薬品コード', 
        how='left'
    ).merge(
        t[['個別レセ電コード', 'ハイリスク算定対象']],
        on='個別レセ電コード',
        how='left'
    )

#%%
res.columns
# %%
import csv
res[
    [
        'YJコード', 
        '基本漢字名称', 
        '医薬品名・規格名_カナ名称', 
        '一般名',
        '個別レセ電コード',
        '薬価基準収載医薬品コード',
        '区分',
        '規格単位',
        '販売会社',
        '製造会社',
        '新又は現金額',
        '後発品',
        '算定対象となる後発品', 
        '先発品', 
        '後発品がある先発品'
    ]
].to_csv('./basic_drug_info.csv', index=False)
# %%
set(u['各先発医薬品の後発医薬品の有無に関する情報'])
# %%
